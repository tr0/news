---
title: "Дослідження i2c"
date: 2019-02-12T16:22:00+02:00
draft: false
tags: [NanoPi Neo Air,i2c,pull-up,H3,NanoPi M1,LCD1602]
---

## Огляд схем

В документі [схема Air на оф.сайті](http://wiki.friendlyarm.com/wiki/images/9/98/NanoPi-NEO-Air-1608-Schematic.pdf) на сторінці 5 є зазначені наступні піни:

* `PL0/S_TWI_SCK/S_PL_EINT0`
* `PL1/S_TWI_SDA/S_PL_EINT1`

що є i2c шиною на шині камери:

![PL0, PL1](pl0.png)

Ці ж піни описані в [документації по H3 (Allwinner H3 Datasheet V1.2.pdf - 614 pages, 7MB, 2015-04-23)](http://linux-sunxi.org/images/4/4b/Allwinner_H3_Datasheet_V1.2.pdf), сторінка 77:


![PL0 на H3](pl0_h3.png)

вказано наявність pull-up резисторів.

В той час як на інших пінах (PA11--PA12, які ми використовуємо) немає резисторів:

![PA11, PA12](pa11-12.png)

## Огляд документації до WiringNP

В [статті](http://wiki.friendlyarm.com/wiki/index.php/WiringNP:_NanoPi_NEO/NEO2/Air_GPIO_Programming_with_C) про використання WiringNP (адаптацію WiringPi для NanoPi) сказано:

> The parameter pud can be: PUD_OFF, (no pull up/down), PUD_DOWN (pull to ground) or PUD_UP (pull to 3.3v).
The internal pull up/down resistors have a value of approximately 100KΩ on the NanoPi M1.

Якщо подивитися на [схему NanoPi M1](http://wiki.friendlyarm.com/wiki/images/1/1e/Schematic_NanoPi-M1-V1.1_1804.pdf) (сторіна 5), 

![PA11, PA12](m1.png)

там на пінах PL0/S_TWI_SCK, PL1/S_TWI_SDA встановлено резистори на 10KΩ: `R227`, `R228`.

## Модуль Matrix - I2C LCD1602 Keypad
Прилад який також продають ті ж самі виробникики [Matrix - I2C LCD1602 Keypad](http://wiki.friendlyarm.com/wiki/index.php/Matrix_-_I2C_LCD1602_Keypad), містить модуль [MCP23017](http://ww1.microchip.com/downloads/en/devicedoc/20001952c.pdf).

На сторінці 22 в розділі 3.5.7. описано:

>The GPPU register controls the pull-up resistors for the port pins. If a bit is set and the corresponding pin is configured as an input, the corresponding port pin is internally pulled up with a 100kΩ resistor.
