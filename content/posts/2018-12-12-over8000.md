---
title: "[OP-1] Over 8000 price!"
date: 2018-12-12
tags: [op-1,price]
---

According to [official TE FB page][1]: 
![price](te_ebay_price.jpg)

there were eBay listing for OP-1 for over 8000USD!

* * *

> no one should pay this price for an OP-1.
so hang in there.

> last week a rumor circulated that the OP-1 was dead. no one was more surprised than us.

> however, we’ve been working hard on updating the hardware platform in order to keep the product alive for many years to come. this is causing a brief stop in the production, but we will be up and running at full speed shortly. 
OP-1 4-ever 


[1]: https://www.facebook.com/teenageengineering/photos/a.10150111818101550/10156850413136550/?type=3&theater
