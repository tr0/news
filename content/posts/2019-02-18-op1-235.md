---
title: "[OP-1] Update 235"
date: 2019-02-18
tags: [op-1,update]
---

Update available at [official site][1]. [Download update.][2]

* * *

**New update features:**

* shift + stop toggles hi-res tape grid for editing
* when in external sync sequencers are now started in sync with the beat

**Fixed bugs:**

* pattern sequencer did not always play correctly when triggered from external MIDI: fixed.
* dropping a synth sample patch on a synth slot could under some circumstances generate an assert: fixed.
* when lifting clips from a loop sometimes the cut was not perfect: fixed.
* tape/mixer mutes was not faded like tape solo is: fixed.

* * *

![Update screen](update2.jpg)

![Boot screen](update1.jpg)


[1]: https://www.teenageengineering.com/downloads/op-1
[2]: https://teenage.engineering/_software/op-1/op1_235.op1
