---
title: Pull-up resistors
date: 2018-12-30
draft: false
tags: [i2c,pull-up,resistors]
---

![Our i2c pullups](pullups_i2c.jpg)



* * *

![Example of Pull-up resistor](pullup-resistor.jpg)

* [Pull-up resistor on Wiki][1]
* [Hardware fundamentals: how pull-down and pull-up resistors work][2]
* [Pull-up Resistors on Sparkfun][3]
* [Difference Between Pull-up and Pull-down Resistors and Practical Examples][4]
* [What does pull up resistor means?][5]
* [What is the function of a pull up resistor?][6]



[1]: https://en.wikipedia.org/wiki/Pull-up_resistor
[2]: https://medium.freecodecamp.org/a-simple-explanation-of-pull-down-and-pull-up-resistors-660b308f116a
[3]: https://learn.sparkfun.com/tutorials/pull-up-resistors/all
[4]: https://www.elprocus.com/pull-up-and-pull-down-resistors-with-applications/
[5]: https://www.quora.com/What-does-pull-up-resistor-means
[6]: https://www.quora.com/What-is-the-function-of-a-pull-up-resistor
