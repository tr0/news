---
title: Test backlight with FBTFT
date: 2018-12-03
draft: false
tags: [i2c,touch,spi,fbtft]
---

<video controls src="doc_2019-02-19_16-34-00.mp4"></video>

* * *

* [fbtft library repo][https://github.com/notro/fbtft]
* [fbtft wiki][https://github.com/notro/fbtft/wiki]
* [backlight][https://github.com/notro/fbtft/wiki/Backlight]

* * *

Most drivers use the fbtft backlight support to control backlight.
This is enabled if there is a 'led' gpio present.

These drivers will have backlight power control exposed in the sysfs filesystem.

```bash
# Turn off backlight
echo 1 | sudo tee /sys/class/backlight/*/bl_power

# Turn on backlight
echo 0 | sudo tee /sys/class/backlight/*/bl_power
```

Brightness is currently not supported (need a kernel PWM driver).
