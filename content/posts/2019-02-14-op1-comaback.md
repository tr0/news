---
title: "[OP-1] Come back"
date: 2019-02-14
tags: [op-1,comeback,te,price]
---

According to TE official [Instagram][1], **true love never dies**:

> after being out of stock for more than a year with rumours of its demise, we are very happy to let you know that finally, the OP-1 is back and here to stay! 

* * *

According to official [store page][2], its price is

> OP-1 portable synthesizer € 1 399

* * *

* [Teenage Engineering OP-1 Synthesizer: The Reasons Of The New Price Are Now Known!][3]
* [This is why Teenage Engineering’s OP-1 just got a huge price hike][4]


[1]: https://www.instagram.com/p/Bt3Bra7lIs1/
[2]: https://www.teenageengineering.com/store
[3]: http://www.synthanatomy.com/2019/02/teenage-engineering-op-1-synthesizer-is-back-with-a-50-price-rise.html
[4]: https://www.musicradar.com/news/this-is-why-teenage-engineerings-op-1-just-got-a-huge-price-hike
