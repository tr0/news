---
title: Тест роботи тач-скріна на RPi Zero W
date: 2019-01-01
draft: false
tags: [i2c,touch,RPi,RPi Zero W,ft5406,ft54xx]
---


<video controls src="IMG_0610.MOV"></video>

[Адаптована версія][1] драйвера [FT5406 що працює як bash скрипт][2]. 
Для запуску і роботи достатньо запуску [скрипта][3].


[1]: https://github.com/troyane/ft5406-capacitive-touch
[2]: https://github.com/optisimon/ft5406-capacitive-touch
[3]: https://github.com/troyane/ft5406-capacitive-touch/blob/master/userspace_con_touch_driver.sh
